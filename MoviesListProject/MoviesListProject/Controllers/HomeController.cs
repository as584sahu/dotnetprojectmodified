﻿using MoviesListProject.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Linq.Dynamic;


namespace MoviesListProject.Controllers
{
    public class HomeController : Controller
    {
        MoviesEntities1 db = new MoviesEntities1();

            public ActionResult Index(int page=1, string sort="MovieName", string sortdir="asc", string search= "")
        {
            int pageSize = 10;
            int totalRecord = 0;
            if (page < 1) page = 1;
            int skip = (page * pageSize) - pageSize;
            var data = GetList(search, sort, sortdir, skip, pageSize, out totalRecord);
            ViewBag.TotalRows = totalRecord;
            ViewBag.search = search;
            return View(data);
        }

        public List<MOviesTable> GetList(string search, string sort, string sortdir,int skip, int pageSize, out int totalRecord)
        {

                var v = db.MOviesTables.Where(x => x.MovieName.Contains(search) || x.MovieActor.Contains(search) || x.MovieDirector.Contains(search));

                    totalRecord = v.Count();

                    v = v.OrderBy(sort + " " + sortdir);
                    if (pageSize > 0)
                    {
                        v = v.Skip(skip).Take(pageSize);
                    }


                    return v.ToList();
                
            
        }




        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(MOviesTable movies)
        {
            try
            {

                if (ModelState.IsValid)
                {
                    db.MOviesTables.Add(movies);
                    db.SaveChanges();
                    TempData["Msg"] = "movie created successfully";
                    return RedirectToAction("Create");

                }
                else
                {
                    return View();
                }
            }
            catch(Exception ex)
            {
                TempData["Msg"] = "movie created failed" +ex.Message;

                return RedirectToAction("Create");
            }
        }

        public ActionResult Details(int id)
        {
            try
            {
                if (id != null)
                {
                    return View(db.MOviesTables.Where(x => x.Movieid == id).FirstOrDefault());
                }
                else
                {
                    return View();
                }
            }
            catch (Exception ex)
            {

                Console.WriteLine("detail not found" +ex);
                return View();
            }
            }



        public ActionResult Edit(int id)
        {
            return View(db.MOviesTables.Where(x => x.Movieid == id).FirstOrDefault());
        }

        [HttpPost]
        public ActionResult Edit(int id, MOviesTable movies)
        {
            try
            {
                if (id != null)
                {
                    db.Entry(movies).State = EntityState.Modified;
                    db.SaveChanges();
                    TempData["Msg"] = "movie edit successfully";


                    return RedirectToAction("Edit");
                }
                else
                {
                    return View();
                }
            }

            catch(Exception ex) 
            {
                TempData["Msg"] = " failed" + ex.Message;

                return RedirectToAction("Edit");
            }
        }

        public ActionResult Delete(int id)
        {
            try
            {
                if (id != null)
                {
                    var e = db.MOviesTables.Where(x => x.Movieid == id).FirstOrDefault();
                    db.MOviesTables.Remove(e);
                    db.SaveChanges();
                    TempData["Msg"] = "movie deleted successfully";

                    return RedirectToAction("Index");
                }
                else
                {
                    return View();
                }
            }

            catch (Exception ex)
            {
                TempData["Msg"] = "movie delete failed" +ex.Message;

                return RedirectToAction("Index");
            }
   }   

    }
}










