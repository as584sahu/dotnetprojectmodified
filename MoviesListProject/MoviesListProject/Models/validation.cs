﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MoviesListProject.Models
{
    public class validation
    {
        public int Movieid { get; set; }

        public string MovieActor { get; set; }
        
        public string MovieDirector { get; set; }

        public DateTime  MovieReleaseDate { get; set; }

    }
}