﻿using storedprocedure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace storedprocedure.Controllers
{
    public class EmpController : Controller
    {

        DataClasses1DataContext dc = new DataClasses1DataContext();
        // GET: Emp
        public ActionResult Index()
        {
            var getemprecords = dc.crudempselect(null, null, null, null, "Select").ToList();
            return View(getemprecords);
        }

        // GET: Emp/Details/5
        public ActionResult Details(int id)
        {
            var empdeatils = dc.crudempdeatils(id, null, null, null, "Details").Single(x => x.Empid == id);
            return View(empdeatils);


        }

        // GET: Emp/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Emp/Create
        [HttpPost]
        public ActionResult Create(EmpClass collection)
        {
            try
            {
                // TODO: Add insert logic here
                dc.crudempinsert(null, collection.Empname, collection.Email, collection.Salary, "Insert");
                dc.SubmitChanges();

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Emp/Edit/5
        public ActionResult Edit(int id)
        {
            var empdeatils = dc.crudempdeatils(id, null, null, null, "Details").Single(x => x.Empid == id);
            return View(empdeatils);

        }

        // POST: Emp/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, EmpClass collection)
        {
            try
            {
                // TODO: Add update logic here
                dc.crudempupdate(id, collection.Empname, collection.Email, collection.Salary, "Update");
                dc.SubmitChanges();

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Emp/Delete/5
        public ActionResult Delete(int id)
        {
            var empdeatils = dc.crudempdeatils(id, null, null, null, "Details").Single(x => x.Empid == id);
            return View(empdeatils);

        }

        // POST: Emp/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, EmpClass collection)
        {
            try
            {
                // TODO: Add delete logic here
                dc.crudempdelete(id, null, null, null, "Delete");
                dc.SubmitChanges();

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
