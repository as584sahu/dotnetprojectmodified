﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace storedprocedure.Models
{
    public class EmpClass
    {
        public int Empid { get; set; }
        public string Empname { get; set; }
        public string Email { get; set; }
        public int Salary { get; set; }
    }
}